# hotel-mvc #

Repo ini menampung file untuk tugas Pronet II 2014 secara keseluruhan. 


## Requirements ##

* **Virtual Host** - 
Buatlah sebuah virtual host baru (untuk sementara, ada kabar bahwa aplikasi tidak dapat dijalankan tanpa virtual host). Unduh semua file yang ada di repository ini dan masukkan ke folder virtual host anda.
* ** HTTP Server ** dengan PHP dan ekstensinya 
* ** MySQL ** dan Database usu_pronet2 


## Konfigurasi ##

Ubahlah konfigurasi pada system/config/settings.php. 
```
#!php
<?php

return [
    'baseUrl' => 'http://it.hotel',
    'defaultController' => ''
];
```

Ubahlah konfigurasi pada system/config/database.php sesuai dengan konfigurasi database anda.

```
#!php
<?php
return [
    'DB_SERVER' => 'localhost',
    'DB_USERNAME' => 'root',
    'DB_PASSWORD' => '',
    'DB_NAME' => 'usu_pronet2',
];
```

# MVC (Model-View-Controller) #

Repo ini menggunakan arsitektur MVC. Silahkan pelajari terlebih dahulu cara bekerja MVC melalui http://bfy.tw/6LWu.

## Controller ##

Anda hanya perlu membuat sebuah file PHP baru dengan nama yang sama dengan nama controller yang akan anda buat. Nama file yang anda berikan haruslah **dengan menggunakan huruf kecil (lowercase)**. Nama controller dapat berupa camelCase atau cara penamaan favorit anda.


```
#!php

<?php

namespace Controller;

class Site
{
    public function defaultAction() { return 'index'; }

    public function index()
    {
        include 'view/me.php';
    }
}

?>

```

## Model ##

Untuk sementara, model hanya memiliki parent yang mengambil interface MySQLi dan menyimpannya untuk model tersebut. Untuk mengakses MySQLi pada Model, cukup mengakses $this->db melalui model. 

## View ##

View dapat diletakkan pada folder view atau folder kesayangan anda. Saat ini, belum ada fungsi untuk melakukan rendering view. 

# Changelogs #
