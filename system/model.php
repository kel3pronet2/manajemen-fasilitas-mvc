<?php

namespace System;

class Model
{
    protected $db;
    
    public function __construct()
    {
        $system = KomA::app();
        $this->db = $system->db();
    }
    public function escape($a){
        return $this->db->real_escape_string($a);
    }

}
