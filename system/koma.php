<?php

namespace System;

class KomA
{
    private $webConfig, $dbConfig;

    private static $instance;
    private $currentController, $currentAction;

    private $db;
    
    /**
     * KomA constructor.
     *
     * Menghindari inisialisasi dari luar class KomA dan mengambil
     * file konfigurasi dari system/config
     */

    private function __construct() { }

    public static function app()
    {
        if (! isset(static::$instance)) {
            static::$instance = new KomA();
            static::$instance->initClass();
        }
        return static::$instance;

    }

    private function initClass() {
        spl_autoload_register();

        if (file_exists('system/config/settings.php'))
            $this->webConfig = (include 'system/config/settings.php');
        if (file_exists('system/config/database.php')) {
            $this->dbConfig = (include 'system/config/database.php');
            $this->init_db();
        }
        $this->routeURI();
    }

    public function __destruct()
    {
        @$this->db->close();
    }

    final public function __clone()
    {
        throw new Exception('This is a Singleton. Clone is forbidden');
    }

    final public function __wakeup()
    {
        throw new Exception('This is a Singleton. __wakeup usage is forbidden');
    }

    /**
     * getInstance untuk mengambil class KomA yang sudah atau baru saja
     * dibuat. Hanya satu instance yang dapat digunakan dalam sekali
     * ekseksui
     * @return self
     */

    public function base_url() {
        return $this->webConfig['baseUrl'];
    }

    public function site_url($url) {
        $e = explode('/', $url);
        if(count($e) == 0)
            return $this->base_url();
        if(class_exists("\\Controller\\".$e[0]))
            return $this->base_url()."/".implode('/', $e);
        else
            return $this->base_url();
    }

    public function redirect($url) {
        if(filter_var($url, FILTER_VALIDATE_URL) == TRUE)
            header("Location: ".$url);
    }

    private function init_db() {
        if (!isset($this->db) || $this->db === NULL)
            $this->db = new \mysqli($this->dbConfig['DB_SERVER'], $this->dbConfig['DB_USERNAME'],
                $this->dbConfig['DB_PASSWORD'], $this->dbConfig['DB_NAME']);

        if (mysqli_connect_errno())
            throw new \Exception("Error connecting to database. " . mysqli_connect_error());
    }

    public function db() {
        return $this->db;
    }

    private function routeURI()
    {
        $route = explode("/", parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
        if(count($route) <= 1 || empty($route[1])) {
            if(isset($this->webConfig['defaultController']))
//                $route[1] = $this->webConfig['defaultController'];
                $this->redirect($this->site_url($this->webConfig['defaultController']));
        }
        if(class_exists('\\Controller\\'.$route[1])) {
            $tujuanDalamString = "\\Controller\\".$route[1];
            $objekTujuan = new $tujuanDalamString();

            if(empty($route[2]) && method_exists($objekTujuan, 'defaultAction'))
                $route[2] = $objekTujuan->defaultAction();

            if(method_exists($objekTujuan, $route[2])) {
                $argument = array_splice($route, 3);
                $this->currentController = strtolower($route[1]);
                $this->currentAction = strtolower($route[2]);
                return call_user_func_array([$objekTujuan, $route[2]], $argument);
            }
        }

    }

    public function show_error($title, $message) {
        include "view/error.php";
    }

}