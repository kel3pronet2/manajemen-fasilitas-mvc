<?php
/**
 * Created by PhpStorm.
 * User: rex
 * Date: 6/18/16
 * Time: 7:14 PM
 */

namespace Model;

use System\Model;

class RecommendationModel extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getByCategoryID($catID = -1) {
        $q  = $this->db->prepare("SELECT * FROM tempat_kategori JOIN tempat ON (tempat_kategori.Id_Rekomendasi = tempat.Id_Rekomendasi) JOIN kategori ON (tempat_kategori.Id_Kategori = kategori.Id_Kategori) WHERE ID ".($catID == -1 ? ' >= ' : ' = ')." ?");
        $q->bind_param('i', $catID);
        if($q->execute()) {
            $r  = $q->get_result();
            if($r !== FALSE) {
                while($row = $r->fetch_assoc()) {
                    $arrRes[$row['Id_Rekomendasi']] = $row;
                }
                return $arrRes;
            }
        }
        return FALSE;
    }

    public function getCategoryList($limit = 0, $page = -1) {
        $q = $this->db->prepare("SELECT * FROM kategori ". (($limit <= 0) ? "" : "LIMIT ?,?"));
        if($limit > 0) {
            $realOffset = $limit * $page;
            $q->bind_param('ii', $realOffset, $limit);
        }
        if($q->execute()) {
            $r = $q->get_result();
            if($r !== FALSE) {
                while($row = $r->fetch_assoc()) {
                    $arrRes[$row['Id_Kategori']] = $row;
                }
                return $arrRes;
            }
        }
        return FALSE;
    }
}