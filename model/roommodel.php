<?php

	namespace Model;

	use System\Model;

	class RoomModel extends Model {

		public $data;

		public function __construct() {
			parent::__construct();
		}

		public function getRoomTypeList() {
			$result = $this->db->query("SELECT name, price, picture FROM room_type");

			if($result->num_rows > 0) {
				while($rows = $result->fetch_assoc()) {
					$this->data[]=$rows;
				}
				return $this->data;
			} else {
				echo "Tidak ada data!";
			}
		}

		public function getRoomDetail($id) {
			$this->data = $this->db->query("SELECT id AS type_id, price, feature FROM room_type WHERE name='".ucwords(preg_replace('/-/', ' ', $id))."'")->fetch_assoc();
			$this->data[id] = ucwords(preg_replace('/-/', ' ', $id));

			$result = $this->db->query("SELECT picture FROM room_gallery WHERE type_id=".$this->data[type_id]);
			while($rows = $result->fetch_assoc()) {
				$this->data[picture][]=$rows[picture];
			}
			return $this->data;
		}
	}
?>
