<?php

	namespace Model;

	use System\Model;

	class FacilityModel extends Model {

		public $data;

		public function __construct() {
			parent::__construct();
		}

		public function getFacility() {
			$result = $this->db->query("SELECT * FROM fasilitas");

			if($result->num_rows > 0) {
				while($rows = $result->fetch_assoc()) {
					$this->data[]=$rows;
				}
				return $this->data;
			} else {
				echo "Tidak ada data!";
			}
		}
		
		public function inFacility($nama_facility, $harga, $satuan, $deskripsi,$gambar) {
		
			$dbnama = ($nama_facility != NULL)?"'".$this->escape($nama_facility)."'":'NULL';
			$dbharga = ($harga != NULL)?"'".$this->escape($harga)."'":'NULL';
			$dbsatuan = ($satuan != NULL)?"'".$this->escape($satuan)."'":'NULL';
			$dbdeskripsi = ($deskripsi != NULL)?"'".$this->escape($deskripsi)."'":'NULL';
			$dbgambar = ($gambar != NULL)?"'".$this->escape($gambar)."'":'NULL';
		
			$this->db->query("insert into fasilitas (Id_fasilitas,nama_fasilitas,Harga,Satuan,Deskripsi,image) values('F006',$dbnama, $dbharga, $dbsatuan, $dbdeskripsi,$dbgambar)");
			return $this->db->insert_id;
		}
		
		public function updateFacility($nama_facility, $harga, $satuan, $deskripsi,$gambar) {
			
			$dbId = $this->escape($id);
			$dbnama = ($nama_facility != NULL)?"'".$this->escape($nama_facility)."'":'NULL';
			$dbharga = ($harga != NULL)?"'".$this->escape($harga)."'":'NULL';
			$dbsatuan = ($satuan != NULL)?"'".$this->escape($satuan)."'":'NULL';
			$dbdeskripsi = ($deskripsi != NULL)?"'".$this->escape($deskripsi)."'":'NULL';
			$dbgambar = ($gambar != NULL)?"'".$this->escape($gambar)."'":'NULL';
		
			$this->db->query("update fasilitas set nama_fasilitas='$dbnama',Harga = 'dbharga', Satuan = '$dbsatuan', Deskripsi='$dbdeskripsi',image='dbgambar' where Id_fasilitas='$dbId'");
			return $this->db->update_id;
		}
		
		public function selectById1($id) {
			$dbId = $this->escape($id);
			$dbres =$this->db->prepare("SELECT * FROM fasilitas WHERE Id_fasilitas='$dbId'");

			if($dbres->execute()) {
				$r = $dbres->get_result();
				if($r !== FALSE) {
					while($row = $r->fetch_assoc()) {
						$arrRes = (object)$row;
					}
					return $arrRes;
				}
			}
			return FALSE;

		}
		
		public function deleteFacility($id)
		{
			$dbId = $this->escape($id);
			$this->db->query("delete from fasilitas WHERE Id_fasilitas='$dbId'");
			return;
		}
		
		public function selectByIdReservasi($idres) {
			$dbId = $this->escape($idres);
			$dbres =$this->db->prepare("SELECT * FROM reservasi WHERE id_reservasi='$dbId'");

			if($dbres->execute()) {
				$r = $dbres->get_result();
				if($r !== FALSE) {
					while($row = $r->fetch_assoc()) {
						$arrRes = (object)$row;
					}
					return $arrRes;
				}
			}
			return FALSE;

		}
		
		public function getReservasi(){
			$result->this->db->query("SELECT id_reservasi FROM reservasi");
			
			if($result->num_rows > 0) {
				while($rows = $result->fetch_assoc()) {
					$this->data[]=$rows;
				}
				return $this->data;
			} else {
				echo "Tidak ada data!";
			}
		} 
		
	}
?>
