<?php

namespace Model;

use System\Model;

class PayrollhrdModel extends Model
{
    public function __construct()
    {
        parent::__construct();
    }
public function getCategoryList($limit = 0, $page = -1) {
        $q = $this->db->prepare("SELECT * FROM kategori ". (($limit <= 0) ? "" : "LIMIT ?,?"));
        if($limit > 0) {
            $realOffset = $limit * $page;
            $q->bind_param('ii', $realOffset, $limit);
        }
        if($q->execute()) {
            $r = $q->get_result();
            if($r !== FALSE) {
                while($row = $r->fetch_assoc()) {
                    $arrRes[] = (object)$row;
                }
                return $arrRes;
            }
        }
        return FALSE;
    }
	public function selectAll($showby , $cari, $limit = 0, $page = -1) {
        if(!isset($cari)) {
			if ( !isset($showby)) {
				$showby = "all";
			}
			$dbOrder =  $this->escape($showby);
			if($showby=="all"){
				$dbres = $this->db->prepare("SELECT * FROM seluruh order by Id_gaji". (($limit <= 0) ? "" : "LIMIT ?,?"));
			}
			else{
				$dbres = $this->db->prepare("SELECT * FROM seluruh where Nama_Divisi like '%$dbOrder%'". (($limit <= 0) ? "" : "LIMIT ?,?"));
			}
		}
		else if(isset($cari)) {
			if ( !isset($showby)) {
				$showby = $cari;
			}
			$dbOrder =  $this->escape($cari);
			if($showby=="all"){
				$dbres = $this->db->prepare("SELECT * FROM seluruh order by Id_gaji". (($limit <= 0) ? "" : "LIMIT ?,?"));
			}
			else{
				$dbres = $this->db->prepare("SELECT * FROM seluruh where Nama_Pengguna like '%$dbOrder%'". (($limit <= 0) ? "" : "LIMIT ?,?"));
			}
		}
		 if($limit > 0) {
            $realOffset = $limit * $page;
            $q->bind_param('ii', $realOffset, $limit);
        }
		if($dbres->execute()) {
            $r = $dbres->get_result();

            if($r !== FALSE) {
                while($row = $r->fetch_assoc()) {
                   $arrRes[$row['Id_gaji']] = (object) $row;
                }

                return $arrRes;
            }
        }
		 
        return FALSE;
    }
	public function selectForLogin($Nama_Pengguna,$Kata_Sandi,$report,$id)
    {
        if (!isset($Nama_Pengguna, $Kata_Sandi)) {
        }
        $dbNama_Pengguna = $this->escape($Nama_Pengguna);
        $dbKata_Sandi = $this->escape($Kata_Sandi);
        $dbreport = $this->escape($report);
        $dbId = $this->escape($id);
        $dbres = $this->db->query("select * from karyawan where Kata_Sandi='$dbKata_Sandi' AND Nama_Pengguna='$dbNama_Pengguna'");
        $rows = $dbres->num_rows;


        if ($rows >= 1) {
            session_start();
            $_SESSION['Nama_Pengguna'] = $Nama_Pengguna;
            $_SESSION['Kata_Sandi'] = $Kata_Sandi;
            $dbres = $this->db->prepare("select * from seluruh where Nama_Pengguna='$dbNama_Pengguna' order by ID desc");
            if ($dbreport == 1) {

                $this->db->query("UPDATE karyawan set status=1 WHERE id='$dbId'");
            }

            if ($dbres->execute()) {
                $r = $dbres->get_result();
                if ($r !== FALSE) {
                    while ($row = $r->fetch_assoc()) {
                        $arrRes = (object)$row;
                    }
                    return $arrRes;
                }
            }
            return FALSE;
        }
    }
	public function selectById($id,$status) {
        $dbId = $this->escape($id);
		$dbstatus = $this->escape($status);
		if($dbstatus==1){
		$this->db->query("UPDATE gaji set Status=1 WHERE ID='$dbId'");
        $this->db->query("UPDATE gaji set Tanggal_Pembayaran=NOW() where ID='$dbId'");
		}
		$dbres = $this->db->prepare("SELECT * FROM seluruh WHERE Id_gaji=$dbId");

 
		if($dbres->execute()) {
			
            $r = $dbres->get_result();
           
			if($r !== FALSE) {
                while($row = $r->fetch_assoc()) {
                    $arrRes = (object)$row;
                }
                return $arrRes;
            }
        }
        return FALSE;

    }
	 public function selectById1($id) {
        $dbId = $this->escape($id);
		$dbres =$this->db->prepare("SELECT * FROM karyawan WHERE ID='$dbId'");

		if($dbres->execute()) {
            $r = $dbres->get_result();
            if($r !== FALSE) {
                while($row = $r->fetch_assoc()) {
                    $arrRes = (object)$row;
                }
                return $arrRes;
            }
        }
        return FALSE;

    }
	 public function selectPosisiByDivisi($id_divisi) {
        $dbIddivisi = $this->escape($id_divisi);
        $dbres = $this->db->prepare("SELECT * FROM posisi WHERE Id_Divisi = '$dbIddivisi'");

		if($dbres->execute()) {
            $r = $dbres->get_result();
            if($r !== FALSE) {
                while($row = $r->fetch_assoc()) {
                    $arrRes[] = (object)$row;
                }
                return $arrRes;
            }
        }
        return FALSE;
    }

    public function insert( $Nama_Pengguna, $Nama_Lengkap, $Kata_Sandi, $Tanggal_Lahir,$Jenis_Kelamin,$Alamat,$Divisi,$Posisi ) {

        $dbNama_Pengguna = ($Nama_Pengguna != NULL)?"'".$this->escape($Nama_Pengguna)."'":'NULL';
        $dbNama_Lengkap = ($Nama_Lengkap != NULL)?"'".$this->escape($Nama_Lengkap)."'":'NULL';
        $dbKata_Sandi = ($Kata_Sandi != NULL)?"'".$this->escape($Kata_Sandi)."'":'NULL';
        $dbTanggal_Lahir = ($Tanggal_Lahir != NULL)?"'".$this->escape($Tanggal_Lahir)."'":'NULL';
		$dbJenis_Kelamin = ($Jenis_Kelamin != NULL)?"'".$this->escape($Jenis_Kelamin)."'":'NULL';
        $dbAlamat = ($Alamat != NULL)?"'".$this->escape($Alamat)."'":'NULL';
        $dbDivisi = ($Divisi != NULL)?"'".$this->escape($Divisi)."'":'NULL';
        $dbPosisi = ($Posisi!= NULL)?"'".$this->escape($Posisi)."'":'NULL';


		if($Posisi=="manager"){
		$this->db->query("INSERT INTO posisi (Nama_Posisi,Akses_Level,Gaji_Pokok,Id_Divisi) VALUES ($dbPosisi,2,7000000,$dbDivisi)");
        }
		else if($Posisi=="member")
		{
		$this->db->query("INSERT INTO posisi (Nama_Posisi,Akses_Level,Gaji_Pokok,Id_Divisi) VALUES ($dbPosisi,0,2000000,$dbDivisi)");
		}
		$this->db->query("INSERT INTO karyawan (Nama_Pengguna, Nama_Lengkap, Kata_Sandi, Tanggal_Lahir,Jenis_Kelamin,Alamat,Anggota_sejak,Id_Posisi) VALUES ($dbNama_Pengguna,$dbNama_Lengkap,$dbKata_Sandi,$dbTanggal_Lahir,$dbJenis_Kelamin,$dbAlamat,now(),Last_Insert_id())");
		return $this->db->insert_id;
    }
	public function insertSallary($id,$deskripsi,$gaji ) {
        echo "haha";
        $dbId = ($id != NULL)?"'".$this->escape($id)."'":'NULL';
        $dbdeskripsi = ($deskripsi != NULL)?"'".$this->escape($deskripsi)."'":'NULL';
        $dbgaji = ($gaji!= NULL)?"'".$this->escape($gaji)."'":'NULL';

        $this->db->query("INSERT INTO gaji (Tambahan_Gaji,Deskripsi,Id_Karyawan) VALUES ($dbgaji,$dbdeskripsi,$dbId)");
        return $this->db->insert_id;
    }

    public function delete($id) {
        $dbId = $this->escape($id);
        $this->db->query("DELETE FROM gaji WHERE id=$dbId");
    }


    
}
