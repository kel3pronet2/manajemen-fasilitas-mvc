<!DOCTYPE html>
<html>
	<head>
		<?php
			$system = \System\KomA::app();
		?>
		<title>KOM-A HOTEL RESORT - ROOM</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="<?php echo $system->base_url() ?>/assets/css/style.css" rel="stylesheet" type="text/css" media="all" />
		<link href="<?php echo $system->base_url(); ?>/assets/css/form.css" rel="stylesheet" type="text/css" media="all" />
		<link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>
		<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>

		<!-- start menu -->
		<link href="<?php echo $system->base_url(); ?>/assets/css/megamenu.css" rel="stylesheet" type="text/css" media="all" />
		<script type="text/javascript" src="<?php echo $system->base_url(); ?>/assets/js/megamenu.js"></script>
		<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
		<!-- end start menu -->

		<!--start slider -->
    <link rel="stylesheet" href="<?php echo $system->base_url(); ?>/assets/css/fwslider.css" media="all">
    <script src="<?php echo $system->base_url(); ?>/assets/js/jquery-ui.min.js"></script>
    <script src="<?php echo $system->base_url(); ?>/assets/js/css3-mediaqueries.js"></script>
    <script src="<?php echo $system->base_url(); ?>/assets/js/fwslider.js"></script>
		<!--end slider -->

		<script src="<?php echo $system->base_url(); ?>/assets/js/jquery.easydropdown.js"></script>
	</head>
	<body>
		<!--TOP HEADER-->
		<div class="header-top">
			<div class="wrap">
				<div class="header-top-left">
					<div class="box">
						<img class="call" src="<?php echo $system->base_url(); ?>/assets/images/numbercall.png" height="30px">
					</div>
					<div class="clear">
					</div>
				</div>
				<div class="cssmenu">
					<ul>
						<li><a href="#"></a></li> |
						<li><a href="login.html">Log In</a></li> |
						<li><a href="sign-up.html">Sign Up</a></li> |
						<li><a href="recervacy.html">BOOK NOW</a></li> |
					</ul>
				</div>
				<div class="clear">
				</div>
			</div>
		</div>
		<!--end TOP HEADER-->

		<!--BOTTOM HEADER-->
		<div class="header-bottom">
	    <div class="wrap">
				<div class="header-bottom-left">
					<div class="logo">
						<a href="#"><img src="<?php echo $system->base_url(); ?>/assets/images/log.png" alt="" height="50px"/></a>
					</div>
					<div class="menu">
						<ul class="megamenu skyblue">
							<li><a href="index.html">HOME</a></li>
							<li><a class="color1" href="recommendation.html">RECOMMENDATION</a></li>
							<li class="active grid"><a class="color2" href="#">ROOM</a></li>
							<li><a class="color3" href="facility.html">FACILITIES</a></li>
							<li><a class="color4" href="about.html">ABOUT US</a></li>
						</ul>
					</div>
				</div>
				<div class="header-bottom-right">
					<div class="search">
						<input type="text" name="s" class="textbox" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}">
						<input type="submit" value="Subscribe" id="submit" name="submit">
						<div id="response"></div>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<!--end BOTTOM HEADER-->

		<!-- start slider -->
    <div id="fwslider">
    	<div class="slider_container">
      	<div class="slide">
        	<!-- Slide image -->
          <img src="<?php echo $system->base_url(); ?>/assets/images/pict/ban3.jpg" alt=""/>
          <!-- /Slide image -->
          <!-- Texts container -->
          <div class="slide_content">
          	<div class="slide_content_wrap">
            	<!-- Text title -->
              <h1 class="title">WELCOME to</h1>
              <!-- /Text title -->
              <!-- Text description -->
              <p class="description">KOM-A Hotel Resort</p>
              <!-- /Text description -->
            </div>
					</div>
          <!-- /Texts container -->
        </div>
        <!-- /Duplicate to create more slides -->
        <div class="slide">
        	<img src="<?php echo $system->base_url(); ?>/assets/images/pict/ban2.jpg" alt=""/>
          <div class="slide_content">
          	<div class="slide_content_wrap">
           		<!-- Text title -->
              <h1 class="title">WELCOME to</h1>
              <!-- /Text title -->
              <!-- Text description -->
              <p class="description">KOM-A Hotel Resort</p>
              <!-- /Text description -->
            </div>
          </div>
        </div>
        <!--/slide -->
			 	<!-- /Duplicate to create more slides -->
        <div class="slide">
        	<img src="<?php echo $system->base_url(); ?>/assets/images/pict/ban1a.jpg" alt=""/>
          <div class="slide_content">
            <div class="slide_content_wrap">
           		<!-- Text title -->
              <h1 class="title">WELCOME to</h1>
              <!-- /Text title -->
              <!-- Text description -->
              <p class="description">KOM-A Hotel Resort</p>
              <!-- /Text description -->
            </div>
          </div>
        </div>
        <!--/slide -->
			</div>
        <div class="timers"></div>
        <div class="slidePrev"><span></span></div>
        <div class="slideNext"><span></span></div>
    </div>
		<!--end SLIDER-->

		<!--list HEADER-->
		<div class="header-list">
	 	</div>
		<!--end list HEADER-->

		<!--main-->
		<div class="main">
		  <div class="wrap">

				<!-- mulai bagusan bagian ini -->
        <table width="100%">
          <tr><td colspan="2"><?php echo $data[id]; ?></td></tr>
          <tr><td>Harga</td><td><?php echo $data[price]; ?></td></tr>
          <tr><td>Fasilitas</td><td>
            <?php
              $a = explode(', ', $data[feature]);
              foreach($a as $row) {
                echo "<li>".$row."</li>";
              }
            ?>
					</td></tr>
					</table>
					<!-- akhir bagusin bagian ini -->

					<?php foreach($data[picture] as $row): ?>
					<div class="col_1_of_3">
						<?php echo "<img src='".$system->base_url()."/assets/images/room/".$row.".jpg' height='200px'>"; ?>
					</div>
					<?php endforeach; ?>

			</div>
			<div class="clear"></div>
		</div>
		<!--end main-->

		<!--footer-->
		<div class="footer" style="top: 1000px">
			<div class="footer-bottom">
				<div class="copy">
					<p>© 2016 Template by. <a href="index.html" target="_blank">KOM-A TI USU 2014</a></p>
				</div>
				<img class="footer_logo" src="<?php echo $system->base_url(); ?>/assets/images/logofooter.png">
				<div class="clear"></div>
			</div>
		</div>
		<!--end footer-->
	</body>
</html>
