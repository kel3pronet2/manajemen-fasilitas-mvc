<html>
<head>
<?php
        $system = \System\KomA::app();
    ?>
	<title>KOM-A HOTEL RESORT</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="assets/css/style.css" rel="stylesheet" type="text/css" media="all" />
	<link href="assets/css/form.css" rel="stylesheet" type="text/css" media="all" />
	<link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>
	<script type="text/javascript" src="assets/js/jquery1.min.js"></script>

	<!--CSS-->
	<style>
	.foto{
		height:100px;
		width:100px;
		border:1px solid;
		text-align:center;
		float:left;
	}
	.deskripsi{
		float:left;
		margin:0 0 0 30px;
		font-weight:bolder;
		background-color:#F5F5F5;
		padding:10px;
	}
	.deskripsi1{
		color:#4689bc;
	}
	.cleaning{
		padding:20px 0 0 0;
		font-weight:bolder;
	}
	.kepala{
		background:rgba(0,0,0,.5);
		color:rgba(255,255,255,1);
		font-weight:bolder;
	}
	
	</style>
    <link href="<?php echo $system->base_url() ?>/assets/css/style.css" rel="stylesheet" type="text/css" media="all" />
	<link href="<?php echo $system->base_url() ?>/assets/css/form.css" rel="stylesheet" type="text/css" media="all" />
	<link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>
	<script type="text/javascript" src="<?php echo $system->base_url() ?>/assets/js/jquery1.min.js"></script>


	<!--CSS-->
    <link rel="stylesheet" href="<?php echo $system->base_url() ?>/assets/bootstrap-3.3.1/dist/css/bootstrap.css">

		<!-- start menu -->
	<link href="<?php echo $system->base_url() ?>/assets/css/megamenu.css" rel="stylesheet" type="text/css" media="all" />
	<script type="text/javascript" src="<?php echo $system->base_url() ?>/assets/js/megamenu.js"></script>
	<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
		<!-- end start menu -->

		<!--start slider -->
    <link rel="stylesheet" href="<?php echo $system->base_url() ?>/assets/css/fwslider.css" media="all">
    <script src="<?php echo $system->base_url() ?>/assets/js/jquery-ui.min.js"></script>
    <script src="<?php echo $system->base_url() ?>/assets/js/css3-mediaqueries.js"></script>
    <script src="<?php echo $system->base_url() ?>/assets/js/fwslider.js"></script>
		<!--end slider -->

	<script src="<?php echo $system->base_url() ?>/assets/js/jquery.easydropdown.js"></script>
</head>
<body>
	<!--TOP HEADER-->
	<div class="header-top">
		<div class="wrap">
			<div class="header-top-left">
				<div class="box">
					<img class="call" src="<?php echo $system->base_url() ?>/assets/images/numbercall.png" height="30px">
				</div>
				<div class="clear">
				</div>
			</div>
			<div class="cssmenu">
				<ul>

					<li><a href="<?php echo $system->site_url('payrollhrd/akhirisesi')?>" style="background-color:black;padding:2px;">Log Out</a></li>
				</ul>
			</div>
			<div class="clear">
			</div>
		</div>
	</div>
	<!--end TOP HEADER-->

	<!--BOTTOM HEADER-->
	<div class="header-bottom">
	    <div class="wrap">
			<div class="header-bottom-left">
				<div class="logo">
					<a href="#"><img src="<?php echo $system->base_url() ?>/assets/images/log.png" alt="" height="50px"/></a>
				</div>
				<div class="menu">
					<ul class="megamenu skyblue">

					</ul>
				</div>
			</div>

			<div class="clear"></div>
		</div>
	</div>
	<!--end BOTTOM HEADER-->

    <!-- start slider -->
    <div id="fwslider">
        <div class="slider_container">
            <div class="slide">
                <!-- Slide image -->
                <img src="<?php echo $system->base_url() ?>/assets/images/pict/ban3.jpg" alt=""/>
                <!-- /Slide image -->
                <!-- Texts container -->
                <div class="slide_content">
                    <div class="slide_content_wrap">
                        <!-- Text title -->
                        <h1 class="title">WELCOME to</h1>
                        <!-- /Text title -->
                        <!-- Text description -->
                        <p class="description">KOM-A Hotel Resort</p>
                        <!-- /Text description -->
                    </div>
                </div>
                <!-- /Texts container -->
            </div>
            <!-- /Duplicate to create more slides -->
            <div class="slide">
                <img src="<?php echo $system->base_url() ?>/assets/images/pict/ban2.jpg" alt=""/>
                <div class="slide_content">
                    <div class="slide_content_wrap">
                        <!-- Text title -->
                        <h1 class="title">WELCOME to</h1>
                        <!-- /Text title -->
                        <!-- Text description -->
                        <p class="description">KOM-A Hotel Resort</p>
                        <!-- /Text description -->
                    </div>
                </div>
            </div>
            <!--/slide -->
            <!-- /Duplicate to create more slides -->
            <div class="slide">
                <img src="<?php echo $system->base_url() ?>/assets/images/pict/ban1a.jpg" alt=""/>
                <div class="slide_content">
                    <div class="slide_content_wrap">
                        <!-- Text title -->
                        <h1 class="title">WELCOME to</h1>
                        <!-- /Text title -->
                        <!-- Text description -->
                        <p class="description">KOM-A Hotel Resort</p>
                        <!-- /Text description -->
                    </div>
                </div>
            </div>
            <!--/slide -->
        </div>
        <div class="timers"></div>
        <div class="slidePrev"><span></span></div>
        <div class="slideNext"><span></span></div>
    </div>
    <!--end SLIDER-->
	
	<!--list HEADER-->
	<div class="header-list">
 	</div>
	<!--end list HEADER-->
	<div class ="container">
<form class="form-horizontal" method="POST" action="">
  <fieldset>
    <legend>Form gaji</legend>
    <div class="form-group">
      <label class="col-md-2 control-label" >Nama_Pengguna</label>

      <div class="col-md-6">
        <?php echo "<td>".$staff->Nama_Pengguna."</td>";	?>
      </div>
    </div>
	<div class="form-group">
      <label class="col-md-2 control-label" >Nama_Lengkap</label>

      <div class="col-md-6">
        <?php echo "<td>".$staff->Nama_Lengkap."</td>";	?>
      </div>
    </div>
		<div class="form-group">
	 <label class="col-md-2 control-label">Deskripsi gaji</label>
	<div class="col-md-6">
        <input class="form-control" id="deskripsi" placeholder="deskripsi" name="deskripsi">	
      </div>
      
    </div>
	<div class="form-group">
	 <label class="col-md-2 control-label">Jumlah gaji</label>
	<div class="col-md-6">
        <input class="form-control" id="gaji" name="gaji" min=1>	
      </div>
      
    </div>
    <div class="form-group">
      <div class="col-md-10 col-md-offset-2">
   
        <button class="btn btn-primary" type="submit" name="form-submitted">submit</button>
      </div>
    </div>
  </fieldset>
</form>

	</div>


	
	<div class="col-sm-4">
	</div>
	</div>
</div>
<br>

	
	<!--footer-->
	<div class="footer">
		<div class="footer-bottom">
			<div class="copy">
				<p>© 2016 Template by. <a href="../index.html" target="_blank">KOM-A TI USU 2014</a></p>	
			</div>
			<img class="footer_logo" src="assets/picture/images/logofooter.png">
			<div class="clear"></div>
		</div>
	</div>
	
</body>
</html>