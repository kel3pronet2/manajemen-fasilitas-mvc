<html>
<head>
<?php
        $system = \System\KomA::app();
    ?>
	<title>KOM-A HOTEL RESORT</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	 <meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
  	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<link href="assets/css/style.css" rel="stylesheet" type="text/css" media="all" />
	<link href="assets/css/form.css" rel="stylesheet" type="text/css" media="all" />
	<link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>
	<script type="text/javascript" src="assets/js/jquery1.min.js"></script>
  	

	<!--CSS-->
    <link rel="stylesheet" href="assets/bootstrap-3.3.1/dist/css/bootstrap.css">
   
		<!-- start menu -->
	<link href="assets/css/megamenu.css" rel="stylesheet" type="text/css" media="all" />
	<script type="text/javascript" src="assets/js/megamenu.js"></script>
	<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
		<!-- end start menu -->
	
		<!--start slider -->
    <link rel="stylesheet" href="assets/css/fwslider.css" media="all">
    <script src="assets/js/jquery-ui.min.js"></script>
    <script src="assets/js/css3-mediaqueries.js"></script>
    <script src="assets/js/fwslider.js"></script>
		<!--end slider -->
		
	<script src="assets/js/jquery.easydropdown.js"></script>
</head>
<body>
	<!--TOP HEADER-->
	<div class="header-top">
		<div class="wrap"> 
			<div class="header-top-left">
				<div class="box">
					<img class="call" src="assets/picture/images/numbercall.png" height="30px">
				</div>
				<div class="clear">
				</div>
			</div>
			<div class="cssmenu">
				<ul>
					
					<li><a href="<?php echo $system->site_url('payrollhrd/akhirisesi')?>" style="background-color:black;padding:2px;">Log Out</a></li> 
				</ul>
			</div>
			<div class="clear">
			</div>
		</div>
	</div>
	<!--end TOP HEADER-->
	
	<!--BOTTOM HEADER-->
	<div class="header-bottom">
	    <div class="wrap">
			<div class="header-bottom-left">
				<div class="logo">
					<a href="#"><img src="assets/picture/images/log.png" alt="" height="50px"/></a>
				</div>
				<div class="menu">
					<ul class="megamenu skyblue">
						
					</ul>
				</div>
			</div>
		
			<div class="clear"></div>
		</div>
	</div>
	<!--end BOTTOM HEADER-->
	
	<!-- start slider -->
    <div id="fwslider">
        <div class="slider_container">
            <div class="slide"> 
                <!-- Slide image -->
                    <img src="assets/picture/pict/ban3.jpg" alt=""/>
                <!-- /Slide image -->
                <!-- Texts container -->
                <div class="slide_content">
                    <div class="slide_content_wrap">
                        <!-- Text title -->
                        <h1 class="title">WELCOME to</h4>
                        <!-- /Text title -->
                        <!-- Text description -->
                        <p class="description">KOM-A Hotel Resort</p>
                        <!-- /Text description -->
                    </div>
                </div>
                 <!-- /Texts container -->
            </div>
            <!-- /Duplicate to create more slides -->
            <div class="slide">
                <img src="assets/picture/pict/ban2.jpg" alt=""/>
                <div class="slide_content">
                    <div class="slide_content_wrap">
                       <!-- Text title -->
                        <h1 class="title">WELCOME to</h4>
                        <!-- /Text title -->
                        <!-- Text description -->
                        <p class="description">KOM-A Hotel Resort</p>
                        <!-- /Text description -->
                    </div>
                </div>
            </div>
            <!--/slide -->
			 <!-- /Duplicate to create more slides -->
            <div class="slide">
                <img src="assets/picture/pict/ban1a.jpg" alt=""/>
                <div class="slide_content">
                    <div class="slide_content_wrap">
                       <!-- Text title -->
                        <h1 class="title">WELCOME to</h4>
                        <!-- /Text title -->
                        <!-- Text description -->
                        <p class="description">KOM-A Hotel Resort</p>
                        <!-- /Text description -->
                    </div>
                </div>
            </div>
            <!--/slide -->
        </div>
        <div class="timers"></div>
        <div class="slidePrev"><span></span></div>
        <div class="slideNext"><span></span></div>
    </div>
	<!--end SLIDER-->
	
	<!--list HEADER-->
	<div class="header-list">
 	</div>
	<!--end list HEADER-->
<br>
<div class ="container">
<form class="form-horizontal" method="POST" action="">
  <fieldset>
    <legend>Form Karyawan</legend>
    <div class="form-group">
      <label class="col-md-2 control-label" for="inputUsername">Nama Pengguna</label>

      <div class="col-md-6">
        <input class="form-control" id="inputUsername" type="username" placeholder="Username" name="Nama_Pengguna">
      </div>
    </div>
	<div class="form-group">
      <label class="col-md-2 control-label" for="inputName">Nama Lengkap</label>

      <div class="col-md-6">
        <input class="form-control" id="inputName" type="name" placeholder="Full Name" name="Nama_Lengkap">
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-2 control-label" for="inputPassword" onautocomplete="off">Kata Sandi</label>

      <div class="col-md-6">
        <input class="form-control" id="inputPassword" type="password" placeholder="Password" name="Kata_Sandi">
</div>
</div>
		<div class="form-group">
            <label class="col-md-2 control-label" for ="lblTglLahir">Tanggal Lahir</label>
            <div class="col-xs-2">
                <select class="form-control" name="Tanggal">
                    <option value="">Tanggal</option> 
              <option value="01">01</option> 
                    <option value="02">02</option> 
                    <option value="03">03</option> 
                    <option value="04">04</option> 
                    <option value="05">05</option> 
                    <option value="06">06</option> 
                    <option value="07">07</option> .                    
					<option value="08">08</option> 
                <option value="09">09</option> 
                    <option value="10">10</option> 
                    <option value="11">11</option> 
                   <option value="12">12</option> 
                    <option value="13">13</option> 
                    <option value="14">14</option> 
                  <option value="15">15</option> 
                    <option value="16">16</option> 
                    <option value="17">17</option> 
                    <option value="18">18</option> 
                    <option value="19">19</option> 
                   <option value="20">20</option> 
                   <option value="21">21</option> 
                    <option value="22">22</option> 
                    <option value="">23</option> 
                    <option value="23">24</option> 
                    <option value="24">25</option> 
                    <option value="26">26</option> 
                    <option value="27">27</option> 
                    <option value="28">28</option> 
                    <option value="29">29</option> 
                    <option value="30">30</option> 
                    <option value="31">31</option> 
                </select>
            </div>
            <div class="col-xs-2">
                <select class="form-control" name="Bulan">
                    <option value="">Bulan</option>
                    <option value="01">Januari</option>
                    <option value="02">Februari</option>
                    <option value="03">Maret</option>
                    <option value="04">April</option>
                    <option value="05">Mei</option>
                    <option value="06">Juni</option>
                    <option value="07">Juli</option>
                    <option value="08">Agustus</option>
                    <option value="09">September</option>
                    <option value="10">Oktober</option>
                    <option value="11">Nopember</option>
                    <option value="12">Desember</option>
                </select>
            </div>
            <div class="col-xs-2">
                <select class="form-control" name="Tahun">
                    <option value="">Tahun</option>
                    <option value="1990">1990</option>
                    <option value="1991">1991</option>
                    <option value="1992">1992</option>
                    <option value="1993">1993</option>
                    <option value="1994">1994</option>
                    <option value="1995">1995</option>
                    <option value="1996">1996</option>
                    <option value="1997">1997</option>
                    <option value="1998">1998</option>
                    <option value="1999">1999</option>
                    <option value="2000">2000</option>
                    <option value="2001">2001</option>
                    <option value="2002">2002</option>
                    <option value="2003">2003</option>
                    <option value="2004">2004</option>
                    <option value="2005">2005</option>
                    <option value="2006">2006</option>
                    <option value="2007">2007</option>
                    <option value="2008">2008</option>
                    <option value="2009">2009</option>
                    <option value="2010">2010</option>
                    <option value="2011">2011</option>
                    <option value="2012">2012</option>
                    <option value="2013">2013</option>
                    <option value="2014">2014</option>
                    <option value="2015">2015</option>
                </select>
            </div>
			</div>
   <div class="form-group">
      <label class="col-md-2 control-label" for="inputAddress">Alamat</label>

      <div class="col-md-6">
        <input class="form-control" id="inputAddress" type="address" placeholder="Address" name="Alamat">	
      </div>
    </div>
	
	 <div class="form-group">
            <label class="col-md-2 control-label for"="lblGender">Jenis_Kelamin</label>
            <div class="col-xs-1">
                <label class="radio-inline">
                    <input type="radio" name="Jenis_Kelamin" value="Male"> Male
                </label>
            </div>
            <div class="col-xs-1">
                <label class="radio-inline">
                    <input type="radio" name="Jenis_Kelamin" value="Female"> Female
                </label>
            </div>
        </div>
		
		<div class="form-group">
	 <label class="col-md-2 control-label" for="select222">Divisi</label>

      <div class="col-md-6">
        <select class="form-control" id="select222" name=Divisi>
          <option value=1>Management</option>
          <option value=2>Front Office</option>
          <option value=3>House Kepping</option>
          <option value=4>Laundry</option>
          <option value=5>Enggineering & Maintenance</option>
		  <option value=6>Food and Beverage</option>
		  <option value=7> Finance</option>
		  <option value=8>Personnel</option>
		  <option value=9>Training</option>
		  <option value=10>Security</option>
        </select>
      </div>
    </div>
	
	 <div class="form-group">
      <label class="col-md-2 control-label">Posisi</label>

      <div class="col-md-6">
        <div class="radio radio-primary">
          <label>
            <input name="Posisi" id="optionsRadios1" type="radio" checked="" value="manager">
            Manager
          </label>
        </div>
        <div class="radio radio-primary">
          <label>
            <input name="Posisi" id="optionsRadios2" type="radio" value="member">
            Member
          </label>
        </div>
      </div>
    </div>
	
    <div class="form-group">
      <div class="col-md-10 col-md-offset-2">
   
        <button class="btn btn-primary" type="submit" name="form-submitted">Tambah</button>
      </div>
    </div>
  </fieldset>
</form>

	</div>
	<!--footer-->
	<div class="footer">
		<div class="footer-bottom">
			<div class="copy">
				<p>© 2016 Template by. <a href="index.html" target="_blank">KOM-A TI USU 2014</a></p>	
			</div>
			<img class="footer_logo" src="assets/picture/images/logofooter.png">
			<div class="clear"></div>
		</div>
	</div>
	
</body>
</html>