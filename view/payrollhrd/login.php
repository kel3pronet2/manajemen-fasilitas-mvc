<html>
<head>
<?php
        $system = \System\KomA::app();
    ?>
	<title>KOM-A HOTEL RESORT - FACILITIES</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<l<link href="<?php echo $system->base_url() ?>/assets/css/style.css" rel="stylesheet" type="text/css" media="all" />
	<link href="<?php echo $system->base_url() ?>/assets/css/form.css" rel="stylesheet" type="text/css" media="all" />
	<link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>
	<script type="text/javascript" src="<?php echo $system->base_url() ?>/assets/js/jquery1.min.js"></script>


	<!--CSS-->
    <link rel="stylesheet" href="<?php echo $system->base_url() ?>/assets/bootstrap-3.3.1/dist/css/bootstrap.css">

		<!-- start menu -->
	<link href="<?php echo $system->base_url() ?>/assets/css/megamenu.css" rel="stylesheet" type="text/css" media="all" />
	<script type="text/javascript" src="<?php echo $system->base_url() ?>/assets/js/megamenu.js"></script>
	<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
		<!-- end start menu -->

		<!--start slider -->
    <link rel="stylesheet" href="<?php echo $system->base_url() ?>/assets/css/fwslider.css" media="all">
    <script src="<?php echo $system->base_url() ?>/assets/js/jquery-ui.min.js"></script>
    <script src="<?php echo $system->base_url() ?>/assets/js/css3-mediaqueries.js"></script>
    <script src="<?php echo $system->base_url() ?>/assets/js/fwslider.js"></script>
		<!--end slider -->

	<script src="<?php echo $system->base_url() ?>/assets/js/jquery.easydropdown.js"></script>
</head>
<body>
	<!--TOP HEADER-->
	<div class="header-top">
		<div class="wrap">
			<div class="header-top-left">
				<div class="box">
					<img class="call" src="<?php echo $system->base_url() ?>/assets/images/numbercall.png" height="30px">
				</div>
				<div class="clear">
				</div>
			</div>
			<div class="cssmenu">
				<ul>

					<li><a href="<?php echo $system->site_url('payrollhrd/akhirisesi')?>" style="background-color:black;padding:2px;">Log Out</a></li>
				</ul>
			</div>
			<div class="clear">
			</div>
		</div>
	</div>
	<!--end TOP HEADER-->

	<!--BOTTOM HEADER-->
	<div class="header-bottom">
	    <div class="wrap">
			<div class="header-bottom-left">
				<div class="logo">
					<a href="#"><img src="<?php echo $system->base_url() ?>/assets/images/log.png" alt="" height="50px"/></a>
				</div>
				<div class="menu">
					<ul class="megamenu skyblue">

					</ul>
				</div>
			</div>

			<div class="clear"></div>
		</div>
	</div>
	<!--end BOTTOM HEADER-->

    <!-- start slider -->
    <div id="fwslider">
        <div class="slider_container">
            <div class="slide">
                <!-- Slide image -->
                <img src="<?php echo $system->base_url() ?>/assets/images/pict/ban3.jpg" alt=""/>
                <!-- /Slide image -->
                <!-- Texts container -->
                <div class="slide_content">
                    <div class="slide_content_wrap">
                        <!-- Text title -->
                        <h1 class="title">WELCOME to</h1>
                        <!-- /Text title -->
                        <!-- Text description -->
                        <p class="description">KOM-A Hotel Resort</p>
                        <!-- /Text description -->
                    </div>
                </div>
                <!-- /Texts container -->
            </div>
            <!-- /Duplicate to create more slides -->
            <div class="slide">
                <img src="<?php echo $system->base_url() ?>/assets/images/pict/ban2.jpg" alt=""/>
                <div class="slide_content">
                    <div class="slide_content_wrap">
                        <!-- Text title -->
                        <h1 class="title">WELCOME to</h1>
                        <!-- /Text title -->
                        <!-- Text description -->
                        <p class="description">KOM-A Hotel Resort</p>
                        <!-- /Text description -->
                    </div>
                </div>
            </div>
            <!--/slide -->
            <!-- /Duplicate to create more slides -->
            <div class="slide">
                <img src="<?php echo $system->base_url() ?>/assets/images/pict/ban1a.jpg" alt=""/>
                <div class="slide_content">
                    <div class="slide_content_wrap">
                        <!-- Text title -->
                        <h1 class="title">WELCOME to</h1>
                        <!-- /Text title -->
                        <!-- Text description -->
                        <p class="description">KOM-A Hotel Resort</p>
                        <!-- /Text description -->
                    </div>
                </div>
            </div>
            <!--/slide -->
        </div>
        <div class="timers"></div>
        <div class="slidePrev"><span></span></div>
        <div class="slideNext"><span></span></div>
    </div>
    <!--end SLIDER-->
	
	<!--list HEADER-->
	<div class="header-list">
 	</div>
	<!--end list HEADER-->
	
	
	<div class="login">
		<div class="col_1_of_login span_1_of_login">
			<div class="login-title">
				<h4 class="title"><center>LOG IN</center></h4>
					<div id="loginbox" class="loginbox">
						<form action="<?php echo $system->site_url('payrollhrd/ShowProfil')?>" method="post" name="login" id="login-form">
							<fieldset class="input">
								<p id="login-form-username">
									<label for="modlgn_username">Nama_Pengguna</label>
									<input id="modlgn_username" type="text" name="Nama_Pengguna" class="inputbox" size="18" autocomplete="off">
								</p>
								<p id="login-form-password">
									<label for="modlgn_passwd">Kata_Sandi</label>
									<input id="modlgn_passwd" type="password" name="Kata_Sandi" class="inputbox" size="18" autocomplete="off">
								</p>
								<div class="remember">
									<p id="login-form-remember">
										<label for="modlgn_remember"><a href="#">Forget Your Password ? </a></label>
									</p>
							
									<input type="submit" name="submit" class="button" value="Login"></a><div class="clear"></div>
								</div>
							</fieldset>
						</form>
					</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	
	<!--footer-->
	<div class="footer">
		<div class="footer-bottom">
			<div class="copy">
				<p>© 2016 Template by. <a href="index.html" target="_blank">KOM-A TI USU 2014</a></p>	
			</div>
			<img class="footer_logo" src="assets/picture/images/logofooter.png">
			<div class="clear"></div>
		</div>
	</div>
	
</body>
</html>
