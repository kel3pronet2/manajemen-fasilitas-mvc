<html>
<head>
    <?php
        $system = \System\KomA::app();
    ?>
    <title>KOM-A HOTEL RESORT - FACILITIES</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="<?php echo $system->base_url() ?>/assets/css/style.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="<?php echo $system->base_url() ?>/assets/css/form.css" rel="stylesheet" type="text/css" media="all"/>
    <link href='http://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    <!-- Bootstrap Framework -->

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <!-- start menu -->
    <link href="<?php echo $system->base_url() ?>/assets/css/megamenu.css" rel="stylesheet" type="text/css" media="all"/>
    <script type="text/javascript" src="<?php echo $system->base_url() ?>/assets/js/megamenu.js"></script>
    <script>$(document).ready(function () {
            $(".megamenu").megamenu();
        });</script>
    <!-- end start menu -->

    <!--start slider -->
    <link rel="stylesheet" href="<?php echo $system->base_url() ?>/assets/css/fwslider.css" media="all">
    <script src="<?php echo $system->base_url() ?>/assets/js/jquery-ui.min.js"></script>
    <script src="<?php echo $system->base_url() ?>/assets/js/css3-mediaqueries.js"></script>
    <script src="<?php echo $system->base_url() ?>/assets/js/fwslider.js"></script>
    <!--end slider -->

    <script src="<?php echo $system->base_url() ?>/assets/js/jquery.easydropdown.js"></script>
</head>
<body>
<!--TOP HEADER-->
<div class="header-top">
    <div class="wrap">
        <div class="header-top-left">
            <div class="box">
                <img class="call" src="<?php echo $system->base_url() ?>/assets/images/numbercall.png" height="30px">
            </div>
            <div class="clear">
            </div>
        </div>
        <div class="cssmenu">
            <ul>
                <li><a href="#"></a></li>
                |
                <li><a href="login.html">Log In</a></li>
                |
                <li><a href="sign-up.html">Sign Up</a></li>
                |
                <li><a href="recervacy.html">BOOK NOW</a></li>
                |
            </ul>
        </div>
        <div class="clear">
        </div>
    </div>
</div>
<!--end TOP HEADER-->

<!--BOTTOM HEADER-->
<div class="header-bottom">
    <div class="wrap">
        <div class="header-bottom-left">
            <div class="logo">
                <a href="#"><img src="<?php echo $system->base_url() ?>/assets/images/log.png" alt="" height="50px"/></a>
            </div>
            <div class="menu">
                <ul class="megamenu skyblue">
                    <li><a href="index.html">HOME</a></li>
                    <li class="active grid"><a class="color1" href="#">RECOMMENDATION</a></li>
                    <li><a class="color2" href="room.html">ROOM</a></li>
                    <li><a class="color3" href="facility.html">FACILITIES</a></li>
                    <li><a class="color4" href="about.html">ABOUT US</a></li>
                </ul>
            </div>
        </div>
        <div class="header-bottom-right">
            <div class="search">
                <input type="text" name="s" class="textbox" value="Search" onfocus="this.value = '';"
                       onblur="if (this.value == '') {this.value = 'Search';}">
                <input type="submit" value="Subscribe" id="submit" name="submit">
                <div id="response"></div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<!--end BOTTOM HEADER-->

<!-- start slider -->
<div id="fwslider">
    <div class="slider_container">
        <div class="slide">
            <!-- Slide image -->
            <img src="<?php echo $system->base_url() ?>/assets/images/pict/ban3.jpg" alt=""/>
            <!-- /Slide image -->
            <!-- Texts container -->
            <div class="slide_content">
                <div class="slide_content_wrap">
                    <!-- Text title -->
                    <h1 class="title">WELCOME to</h1>
                    <!-- /Text title -->
                    <!-- Text description -->
                    <p class="description">KOM-A Hotel Resort</p>
                    <!-- /Text description -->
                </div>
            </div>
            <!-- /Texts container -->
        </div>
        <!-- /Duplicate to create more slides -->
        <div class="slide">
            <img src="<?php echo $system->base_url() ?>/assets/images/pict/ban2.jpg" alt=""/>
            <div class="slide_content">
                <div class="slide_content_wrap">
                    <!-- Text title -->
                    <h1 class="title">WELCOME to</h1>
                    <!-- /Text title -->
                    <!-- Text description -->
                    <p class="description">KOM-A Hotel Resort</p>
                    <!-- /Text description -->
                </div>
            </div>
        </div>
        <!--/slide -->
        <!-- /Duplicate to create more slides -->
        <div class="slide">
            <img src="<?php echo $system->base_url() ?>/assets/images/pict/ban1a.jpg" alt=""/>
            <div class="slide_content">
                <div class="slide_content_wrap">
                    <!-- Text title -->
                    <h1 class="title">WELCOME to</h1>
                    <!-- /Text title -->
                    <!-- Text description -->
                    <p class="description">KOM-A Hotel Resort</p>
                    <!-- /Text description -->
                </div>
            </div>
        </div>
        <!--/slide -->
    </div>
    <div class="timers"></div>
    <div class="slidePrev"><span></span></div>
    <div class="slideNext"><span></span></div>
</div>
<!--end SLIDER-->

<!--list HEADER-->
<div class="header-list">
</div>
<!--end list HEADER-->

<!--Maps and Recommendations-->
<style>
    #recMap {
        max-height: 80%;
        margin: 1em auto;
        transition: all 1s ease;
    }

    #recMap.activated {
        height: 80%;
    }

    .title-map {
        font-size: 2em;
    }

    .desc-map p {
        font-size: 1em;
    }

    .maps h3 {
        text-transform: uppercase;
        color: #777;
    }

    .cat-item img {
        max-width: 3em;
        max-height: 3em;
        vertical-align: middle;
        display: inline-block;
        margin-right: 1em;
    }

    #catList {
        max-height: 70%;
        overflow-y: auto;
        background: RGBA(240, 240, 240, 0.35);
    }

    a .cat-item {
        padding: 0.5em;
        cursor: pointer;
        color: #333;
    }

    a .cat-item:hover {
        background: #EEE;
    }
</style>
<div class="main container-fluid maps">
    <div class="row">
        <div class="col-md-12">
            <h2 style="text-align: center; " class="head">Recommendations</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-md-offset-1">
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBsyBVLC_bZURZKs2wvJtGlDHuE6sDU6gg&"></script>
            <script type="text/javascript">
                var recommendations, json, markers = { }, infowindow = { }, currentPin = -1, catData;
                // Fungsi untuk membuat styling dan tag untuk judul
                function getTitleContent(title, desc) {
                    return "<div><h1 class=\"title-map\">" + title + "</h1><div class=\"desc-map\">" +
                        "<p>" + desc + "</p></div></div>";
                }
                // Fungsi untuk membuat styling dan tag untuk daftar kategori
                function getCategoryHTML(id, icon, name) {
                    return "<a class='map-cat-button' type=button data-map-id='" + id + "'><div class=\"col-md-12 cat-item\"><img class='category_img' src='" + <?php echo json_encode($system->base_url()) ?> + "/" + icon + "' >" + name + "</div>";
                }
                // Inisialisasi map yang akan dieksekusi setiap kali data json diperbaharui menggunakan AJAX
                function initMap() {
                    var i; // Variabel untuk perulangan pada script ini
                    var pos = { }; // pos menyimpan daftar latLng lokasi pada peta
                    for (var key in json) {
                        // Membuat objek LatLng untuk setiap lokasi dan memasukkannya ke dalam array pos
                        pos[key] = (new google.maps.LatLng(parseFloat(json[key].Latitude), parseFloat(json[key].Longitude)));
                    }
                    if (typeof recommendations == 'undefined') { // Membuat map baru hanya jika recommendations belum
                        // diinisialisasi
                        // Menginisialisasi peta recommendations baru
                        recommendations = new google.maps.Map(document.getElementById('recMap'), {
                            center: {lat: 3.566533, lng: 98.659601}, // Menentukan titik tengah
                            zoom: 15,														// Menentukan nilai zoom awal
                            mapTypeId: google.maps.MapTypeId.ROADMAP, // Menentukan tipe peta yang ditamppilkan
                            scrollwheel: false										// Menonaktifkan scroll yang berfungsi untuk
                            // melakukan zomming pada peta menggunakan scroll
                        });
                        $("#recMap").addClass('activated');
                    }
                    for (var key in json) {
                        // Membuat dan memasukkan marker ke dalam peta dan ke dalam array markers
                        markers[key] = (new google.maps.Marker({
                            id: key,
                            position: pos[key],
                            map: recommendations,
                            title: json[key].Nama,
                            icon: new google.maps.MarkerImage(
                                <?php echo json_encode($system->base_url()) ?> + "/" + catData[json[key].Id_Kategori].Lokasi_Gambar,
                                null,
                                null,
                                new google.maps.Point(16, 50),
                                new google.maps.Size(30, 50)
                            )
                        }));
                        // Membuat dan memasukkan InfoWindow baru yang akan digunakan untuk menampilkan informasi
                        // tambahan saat marker diklik
                        infowindow[key] = (new google.maps.InfoWindow({
                            content: getTitleContent(json[key].Nama, (json[key].Deskripsi ? json[key].Deskripsi : "No description."))
                        }));
                        // Menambahkan listener click pada marker agar InfoWindow yang diinginkan muncul
                        markers[key].addListener('click', function () {
                            infowindow[this.id].open(recommendations, this); // Membuka InfoWindow pada lokasi marker pada peta recommendations
                            if (currentPin != -1) // Jika pengguna sudah pernah mengeklik marker pada peta
                                infowindow[currentPin].close(); // Tutup InfoWindow yang sudah dibuka sebelumnya
                            currentPin = this.id; // Simpan id marker terakhir yang dibuka
                            recommendations.panTo(this.getPosition()); // Pindahkan fokus pada peta recommendations ke titik tengah peta
                            // Memakai panTo agar peta memindahkan fokus dari titik sebelumnya ke
                            // titik yang baru. setCenter juga dapat melakukan fungsi ini, akan tetapi
                            // peta recommendations diperbaharui, bukan memindahkan fokus.
                        });
                    }
                }
                $(document).ready(function () {
                    // Memanggil fungsi AJAX untuk mengambil data kategori dari halaman lain secara  sinkron
                    $.ajax({
                        url: '<?php echo $system->site_url('recommendation/get_category') ?>',
                        method: 'GET',
                        dataType: 'json',
                        async: false
                    }).success(function (data) {
                        $("#catList").html(""); // Menghapus seluruh data kategori jika ada
                        // dan menambahkan kategori pada catList (dan juga all categories)
                        $("#catList").append(getCategoryHTML('', 'assets/images/recommendations/all.png', 'All Categories'));
                        for (var key in data) {
                            if (data.hasOwnProperty(key))
                                $("#catList").append(getCategoryHTML(key, data[key].Lokasi_Gambar, data[key].Nama_Kategori));
                        }
                        // Membuat listener click untuk setiap tombol kategori
                        $(".map-cat-button").each(function (i, e) {
                            $(e).on('click', function () {
                                for(var key in markers) {
                                    google.maps.event.clearInstanceListeners(infowindow[key]);
                                    markers[key].setMap(null); // Menghapus markers dan InfoWindow yang lama
                                }
                                markers = {};
                                infowindow = {};
                                // Melakukan panggilan AJAX untuk mengambil daftar recommendations dengan kategori tertentu
                                $.ajax({
                                    url: '<?php echo $system->site_url('recommendation/get_list') ?>',
                                    method: 'GET',
                                    data: {CatID: $(e).data('map-id')},
                                    dataType: 'json',
                                    async: false
                                }).success(function (data) {
                                    json = data; // Memperbaharui data json
                                    initMap(); // Inisialisasi kembali markers untuk recommendations
                                });
                            });
                        });
                        catData = data; // Menyimpan data kategori sehingga tidak perlu meminta berulang-ulang
                    });
                });
            </script>
            <div id="recMap" class="maps-container"></div> <!-- Tag div yang digunakan oleh maps -->
        </div>
        <div class="col-md-4">
            <h3 class="head" style="text-align: center">Categories</h3>
            <div id="catList"></div>
        </div>
    </div>
</div>

<!--footer-->
<div class="footer">
    <div class="footer-bottom">
        <div class="copy">
            <p>© 2016 Template by. <a href="index.html" target="_blank">KOM-A TI USU 2014</a></p>
        </div>
        <img class="footer_logo" src="<?php echo $system->base_url() ?>/assets/images/logofooter.png">
        <div class="clear"></div>
    </div>
</div>

</body>
</html>
