-- phpMyAdmin SQL Dump
-- version 4.6.1
-- http://www.phpmyadmin.net
--
-- Host: 192.168.23.23:3306
-- Generation Time: Jun 21, 2016 at 03:11 PM
-- Server version: 5.5.49-MariaDB-wsrep
-- PHP Version: 5.5.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `usu_pronet2`
--

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `nomor` tinyint(3) NOT NULL,
  `id_reservasi` tinyint(3) NOT NULL,
  `id_room` tinyint(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `divisi`
--

CREATE TABLE `divisi` (
  `ID` mediumint(8) UNSIGNED NOT NULL,
  `Nama_Divisi` varchar(50) DEFAULT NULL,
  `Deskripsi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `divisi`
--

INSERT INTO `divisi` (`ID`, `Nama_Divisi`, `Deskripsi`) VALUES
(1, ' Marketing Departemen', 'Markets several hotels to the marketplace according to their needs '),
(2, 'Front Office Departemen', 'Sell ??rooms are fully qualified and ready to be occupied by hotel guests .'),
(3, 'Housekeeping Departemen ', 'Providing a clean and ready for occupation by hotel guests .'),
(4, 'Enggineering & Maintenance Departemen', 'Mengoperasikan, merawat, dan memperbaiki semua peralatan dalam hotel.'),
(5, 'Laundry Departemen', 'Membantu departemen Housekeeping dalam menyediakan kebutuhan Linen (Handuk, Seprai, Selimut) untuk kamar hotel dan seragam karyawan.'),
(6, 'Food & Beverage Departemen ', 'Menyiapkan makanan dan minuman di dalam hotel.'),
(7, 'Finance Departemen ', 'Mengelola keuangan baik pemasukan maupun pengeluaran hotel.'),
(8, 'Personnel Departemen ', 'Mengurusi seluruh adsministrasi  karyawan hotel.'),
(9, 'Training Departemen ', 'Memberikan berbagai latihan bagi karyawan hotel baik yang baru maupun yang lama.'),
(10, 'Security Departemen', 'Menjaga dan mengatur sistem keamanan hotel');

-- --------------------------------------------------------

--
-- Table structure for table `fasilitas`
--

CREATE TABLE `fasilitas` (
  `Id_fasilitas` varchar(6) NOT NULL,
  `nama_fasilitas` text NOT NULL,
  `Harga` int(15) NOT NULL,
  `Satuan` varchar(10) NOT NULL,
  `deskripsi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fasilitas`
--

INSERT INTO `fasilitas` (`Id_fasilitas`, `nama_fasilitas`, `Harga`, `Satuan`, `deskripsi`) VALUES
('F001', 'Laundry', 8000, 'per Kg', 'Dry Clean (Harga Per Kg)'),
('F002', 'Swimming Pool', 300000, 'per Orang', 'Harga Per Orang maks.4 Jam (Non Tamu Hotel)'),
('F003', 'Sewa Mobil', 800000, 'per Hari', 'Harga Sewa Per Hari (Khusus Tamu Hotel)'),
('F004', 'Delivery Restaurant', 50000, 'per Jasa', 'Harga hanya untuk biaya antar makanan (Pelayanan Delivery).\r\n'),
('F005', 'Pijat Refleksi', 250000, 'per Orang', 'Harga Per Orang (hanya 2,5 Jam)');

-- --------------------------------------------------------

--
-- Table structure for table `gaji`
--

CREATE TABLE `gaji` (
  `ID` mediumint(9) UNSIGNED NOT NULL,
  `Tambahan_Gaji` int(11) NOT NULL,
  `Deskripsi` text NOT NULL,
  `Tanggal_Pembayaran` datetime(1) NOT NULL,
  `Status` bit(1) NOT NULL,
  `Id_Karyawan` mediumint(6) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gaji`
--

INSERT INTO `gaji` (`ID`, `Tambahan_Gaji`, `Deskripsi`, `Tanggal_Pembayaran`, `Status`, `Id_Karyawan`) VALUES
(1, 5000000, 'Gaji diberikan dalam jangka waktu sekali dalam sebulan', '2016-06-04 03:07:08.1', b'1', 1),
(2, 3000000, 'Gaji diberikan dalam jangka waktu sekali dalam sebulan', '2016-06-07 07:19:19.3', b'1', 2),
(3, 1500000, 'Gaji diberikan dalam jangka waktu sekali dalam sebulan', '2016-06-08 04:09:08.1', b'1', 3),
(4, 4000000, 'Gaji diberikan dalam jangka waktu sekali dalam sebulan', '2016-06-01 11:49:36.0', b'1', 4),
(5, 1500000, 'Gaji diberikan dalam jangka waktu sekali dalam sebulan', '2016-06-01 11:49:10.0', b'1', 5);

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE `karyawan` (
  `ID` mediumint(6) UNSIGNED NOT NULL,
  `Nama_Pengguna` varchar(12) NOT NULL,
  `Nama_Lengkap` varchar(50) NOT NULL,
  `Kata_Sandi` varchar(60) NOT NULL,
  `Tanggal_Lahir` date NOT NULL,
  `Jenis_Kelamin` enum('male','female') NOT NULL,
  `Alamat` text NOT NULL,
  `Anggota_sejak` date NOT NULL,
  `forget_key` char(5) NOT NULL,
  `status` bit(1) NOT NULL,
  `Id_Posisi` mediumint(6) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`ID`, `Nama_Pengguna`, `Nama_Lengkap`, `Kata_Sandi`, `Tanggal_Lahir`, `Jenis_Kelamin`, `Alamat`, `Anggota_sejak`, `forget_key`, `status`, `Id_Posisi`) VALUES
(1, 'hetlysaint', 'Hetly Saint Kartika', 'saint002', '1996-06-24', 'female', 'jalan setia budi pasar 1 medan', '2008-05-04', '0', b'0', 1),
(2, 'Wendys', 'Wendy Winata', 'winata001', '1996-01-11', 'male', 'jalan dr mansur no.2a', '2009-09-07', '0', b'1', 2),
(3, 'aggieee', 'Aggie Wicita', 'wicita', '1996-10-09', 'female', 'jalan pembangunan no 8a medan', '2007-09-08', '0', b'0', 3),
(4, 'samuelezzay', 'Ezzay Tarigan', 'ezzay09', '1995-10-05', 'male', 'jalan berdikari no.99', '2008-01-01', '0', b'0', 4),
(5, 'okisalsa22', 'Oki Salsabilla', '123456', '1995-07-30', 'female', 'Jalan Pembangunan No.7', '2014-01-01', '0', b'0', 5);

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `Id_Kategori` tinyint(3) UNSIGNED NOT NULL,
  `Nama_Kategori` varchar(200) NOT NULL,
  `Lokasi_Gambar` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`Id_Kategori`, `Nama_Kategori`, `Lokasi_Gambar`) VALUES
(1, 'KOM A Hotel', NULL),
(2, 'Rumah Sakit', NULL),
(3, 'Tempat Makan', NULL),
(4, 'ATM', NULL),
(5, 'Landmark', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `penggunaan_fasilitas`
--

CREATE TABLE `penggunaan_fasilitas` (
  `Id_fasilitas` varchar(6) NOT NULL,
  `Id_Tamu` varchar(10) NOT NULL,
  `Quantity` int(10) NOT NULL,
  `Total` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `posisi`
--

CREATE TABLE `posisi` (
  `ID` mediumint(6) UNSIGNED NOT NULL,
  `Nama_Posisi` varchar(50) NOT NULL,
  `Akses_Level` tinyint(4) NOT NULL,
  `Gaji_Pokok` int(11) UNSIGNED NOT NULL,
  `Id_Divisi` mediumint(9) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posisi`
--

INSERT INTO `posisi` (`ID`, `Nama_Posisi`, `Akses_Level`, `Gaji_Pokok`, `Id_Divisi`) VALUES
(1, 'Manager', 2, 7000000, 1),
(2, 'Admin', 1, 6000000, 7),
(3, 'Member', 0, 5000000, 1),
(4, 'Manager', 2, 7000000, 3),
(5, 'Member', 0, 4000000, 4);

-- --------------------------------------------------------

--
-- Table structure for table `rating`
--

CREATE TABLE `rating` (
  `Id_Rating` int(11) NOT NULL,
  `Id_Tamu` int(11) NOT NULL,
  `Id_Fasilitas` int(11) NOT NULL,
  `Judul` varchar(200) DEFAULT NULL,
  `Deskripsi` text,
  `Nilai_Rating` int(11) DEFAULT NULL,
  `Waktu_Posting` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `reservasi`
--

CREATE TABLE `reservasi` (
  `id_reservasi` tinyint(3) NOT NULL,
  `id_tamu` tinyint(3) NOT NULL,
  `id_karyawan` mediumint(6) NOT NULL,
  `tgl_booking` date NOT NULL,
  `tgl_checkin` date NOT NULL,
  `tgl_checkout` date NOT NULL,
  `total_pembayaran` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `room`
--

CREATE TABLE `room` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `type_id` tinyint(3) UNSIGNED NOT NULL,
  `status` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `room`
--

INSERT INTO `room` (`id`, `name`, `type_id`, `status`) VALUES
(1, '101', 1, b'1'),
(2, '102', 1, b'1'),
(3, '103', 2, b'1'),
(4, '104', 1, b'1'),
(5, '105', 2, b'1'),
(6, '106', 2, b'0'),
(7, '107', 3, b'0'),
(8, '108', 3, b'1'),
(9, '109', 2, b'1'),
(10, '110', 1, b'1'),
(11, '111', 4, b'0'),
(12, '112', 4, b'0'),
(13, '113', 4, b'1'),
(14, '114', 4, b'1'),
(15, '115', 5, b'1'),
(16, '116', 2, b'0'),
(17, '117', 2, b'1'),
(18, '118', 2, b'0'),
(19, '119', 2, b'1'),
(20, '120', 1, b'1'),
(21, '121', 1, b'1');

-- --------------------------------------------------------

--
-- Table structure for table `room_gallery`
--

CREATE TABLE `room_gallery` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `type_id` tinyint(3) UNSIGNED NOT NULL,
  `picture` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `room_gallery`
--

INSERT INTO `room_gallery` (`id`, `type_id`, `picture`) VALUES
(1, 1, 'standard'),
(2, 1, 'standard_coffee'),
(3, 1, 'standard_toilet'),
(4, 2, 'deluxe'),
(5, 2, 'deluxe_sofa'),
(6, 2, 'deluxe_toilet'),
(7, 3, 'junior'),
(8, 3, 'junior_sofa'),
(9, 3, 'junior_toilet'),
(10, 4, 'suite'),
(11, 4, 'suite_bath'),
(12, 4, 'suite_bed'),
(13, 4, 'suite2'),
(14, 5, 'presidential'),
(15, 5, 'presidential_bath'),
(16, 5, 'presidential_bath2'),
(17, 5, 'presidential_fam'),
(18, 5, 'presidential_kitc'),
(19, 5, 'presidential_work');

-- --------------------------------------------------------

--
-- Table structure for table `room_type`
--

CREATE TABLE `room_type` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `price` int(11) NOT NULL,
  `feature` text NOT NULL,
  `picture` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `room_type`
--

INSERT INTO `room_type` (`id`, `name`, `price`, `feature`, `picture`) VALUES
(1, 'Standart Room', 450000, 'Single bed king-size, Television, Wardrobe, Sofa, Night table, Night table lamp, Coffee maker, Dressing chair, Dressing table, Telephone, Table, Bathroom, Refrigerator', 'standart'),
(2, 'Deluxe Room', 550000, 'Single bed king-size, Television, Wardrobe, Luggage bench, Sofa, Night table, Night table lamp, Foot lamp, Coffee maker, Water heater, Dressing chair, Dressing table, Telephone, Table, Bathroom, Refrigerator', 'deluxe'),
(3, 'Junior Suite Room', 700000, 'Double bed king-size, Television, Wardrobe, Luggage bench, Sofa, Night table, Night table lamp, Foot lamp, Drawer, Coffee table, Dressing chair, Dressing table, Telephone, Table, Bathroom (bathup), Refrigerator', 'junior'),
(4, 'Suite Room', 950000, 'Double room, Double bed king-size, Television, Wardrobe, Luggage bench, Sofa, Night table, Night table lamp, Foot lamp, Ceiling lamp, Drawer, Coffee table, Dressing chair, Dressing table, Arm chair, Telephone, Table, Bathroom (bathup), Family room, Refrigerator', 'suite'),
(5, 'Presidential Room', 1300000, 'Triple room, Double bed king-size, Television, Wardrobe, Luggage bench, Sofa, Night table, Night table lamp, Foot lamp, Ceiling lamp, Drawer, Coffee table, Dressing chair, Dressing table, Arm chair, Telephone, Table, Bathroom (bathup), Family room, Workspace, Kitchen, Refrigerator', 'presidential');

-- --------------------------------------------------------

--
-- Table structure for table `tempat`
--

CREATE TABLE `tempat` (
  `Id_Rekomendasi` smallint(5) UNSIGNED NOT NULL,
  `Nama` varchar(255) NOT NULL,
  `Alamat` text,
  `Deskripsi` text,
  `Longitude` decimal(10,7) NOT NULL,
  `Latitude` decimal(10,7) NOT NULL,
  `Jarak` int(11) DEFAULT NULL,
  `Nilai_Prioritas` smallint(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tempat`
--

INSERT INTO `tempat` (`Id_Rekomendasi`, `Nama`, `Alamat`, `Deskripsi`, `Longitude`, `Latitude`, `Jarak`, `Nilai_Prioritas`) VALUES
(1, 'KOM A Resort Hotel', NULL, NULL, '98.6594960', '3.5627680', 0, 65535),
(2, 'Rumah Sakit Universitas Sumatera Utara', NULL, NULL, '98.6574400', '3.5678560', 1100, 0),
(3, 'Kantin Netral', NULL, NULL, '98.6581230', '3.5624200', 220, 10),
(4, 'Rumah Sakit Siti Hajar', ' Jl. Letjend. Jamin Ginting No. 2 Medan Baru, Kota Medan, Sumatera Utara, Indonesia', NULL, '98.6608300', '3.5727410', 1200, 10);

-- --------------------------------------------------------

--
-- Table structure for table `tempat_kategori`
--

CREATE TABLE `tempat_kategori` (
  `Id` smallint(5) UNSIGNED NOT NULL,
  `Id_Rekomendasi` smallint(5) UNSIGNED NOT NULL,
  `Id_Kategori` tinyint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tempat_kategori`
--

INSERT INTO `tempat_kategori` (`Id`, `Id_Rekomendasi`, `Id_Kategori`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3),
(4, 4, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`nomor`);

--
-- Indexes for table `fasilitas`
--
ALTER TABLE `fasilitas`
  ADD PRIMARY KEY (`Id_fasilitas`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`Id_Kategori`);

--
-- Indexes for table `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`Id_Rating`);

--
-- Indexes for table `reservasi`
--
ALTER TABLE `reservasi`
  ADD PRIMARY KEY (`id_reservasi`);

--
-- Indexes for table `room`
--
ALTER TABLE `room`
  ADD PRIMARY KEY (`id`),
  ADD KEY `room_ibfk_1` (`type_id`);

--
-- Indexes for table `room_gallery`
--
ALTER TABLE `room_gallery`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type_id` (`type_id`);

--
-- Indexes for table `room_type`
--
ALTER TABLE `room_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tempat`
--
ALTER TABLE `tempat`
  ADD PRIMARY KEY (`Id_Rekomendasi`);

--
-- Indexes for table `tempat_kategori`
--
ALTER TABLE `tempat_kategori`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Id_Rekomendasi` (`Id_Rekomendasi`),
  ADD KEY `Id_Kategori` (`Id_Kategori`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `nomor` tinyint(3) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `Id_Kategori` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `rating`
--
ALTER TABLE `rating`
  MODIFY `Id_Rating` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `reservasi`
--
ALTER TABLE `reservasi`
  MODIFY `id_reservasi` tinyint(3) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `room`
--
ALTER TABLE `room`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `room_gallery`
--
ALTER TABLE `room_gallery`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `room_type`
--
ALTER TABLE `room_type`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tempat`
--
ALTER TABLE `tempat`
  MODIFY `Id_Rekomendasi` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tempat_kategori`
--
ALTER TABLE `tempat_kategori`
  MODIFY `Id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `room`
--
ALTER TABLE `room`
  ADD CONSTRAINT `room_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `room_type` (`id`);

--
-- Constraints for table `room_gallery`
--
ALTER TABLE `room_gallery`
  ADD CONSTRAINT `room_gallery_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `room_type` (`id`);

--
-- Constraints for table `tempat_kategori`
--
ALTER TABLE `tempat_kategori`
  ADD CONSTRAINT `tempat_kategori_ibfk_1` FOREIGN KEY (`Id_Rekomendasi`) REFERENCES `tempat` (`Id_Rekomendasi`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tempat_kategori_ibfk_2` FOREIGN KEY (`Id_Kategori`) REFERENCES `kategori` (`Id_Kategori`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
