<?php

	
   set_include_path(implode(PATH_SEPARATOR, [
        './system/',
        './controller/facility',
        './model/',
    ]));

    require_once 'system/koma.php';

    $app = \System\KomA::app();