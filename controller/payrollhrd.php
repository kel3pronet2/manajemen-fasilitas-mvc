<?php

namespace Controller;

use Model\PayrollhrdModel;
use System\KomA;

class Payrollhrd
{
    public $payrollhrdmodel;

    public function defaultAction() { return 'index'; }

    public function __construct()
    {
        $this->payrollhrdmodel = new PayrollhrdModel();
    }

    public function index()
    {
        include 'view/payrollhrd/login.php';
    }

	public function lists() {
       $showby = isset($_GET['showby'])?$_GET['showby']:NULL;
       $cari = isset($_GET['cari'])?$_GET['cari']:NULL;
		$staffs = $this->payrollhrdmodel->selectAll($showby,$cari, $limit, $page);
        include 'view/payrollhrd/staff.php';
    }
	public function sallary() {
       $id = isset($_GET['id'])?$_GET['id']:NULL;

        if ( !$id ) {
            throw new \Exception('Internal error.');
        }
        $Nama_Pengguna = '';
        $Nama_Lengkap = '';
        $deskripsi='';
		$gaji=0;

        $errors = array();

        if ( isset($_POST['form-submitted']) ) {
            $Nama_Pengguna       = isset($_POST['Nama_Pengguna']) ?   $_POST['Nama_Pengguna']  :NULL;
            $Nama_Lengkap      = isset($_POST['Nama_Lengkap'])?   $_POST['Nama_Lengkap'] :NULL;
            $deskripsi     = isset($_POST['deskripsi'])?   $_POST['deskripsi'] :NULL;
            $gaji    = isset($_POST['gaji'])? $_POST['gaji']:NULL;

            try {
                $this->payrollhrdmodel->insertSallary($id,$deskripsi,$gaji);
                return;
            } catch (\Exception $e) {
                $errors = $e->getErrors();
            }
        }
		$staff = $this->payrollhrdmodel->selectById1($id);


        include 'view/payrollhrd/sallary.php';
	}

    public function saveContact() {
 $system= KomA::app();
        $title = 'Add new staff';

        $Nama_Pengguna = '';
        $Nama_Lengkap = '';
        $Kata_Sandi = '';
        $Tanggal_Lahir = '';
		$Jenis_Kelamin = '';
        $Alamat = '';
        $Anggota_sejak = '';
		$forget_key=0;
		$status=1;
		$Id_Posisi=0;

        $errors = array();

        if ( isset($_POST['form-submitted']) ) {

            $Nama_Pengguna       = isset($_POST['Nama_Pengguna']) ?   $_POST['Nama_Pengguna']  :NULL;
            $Nama_Lengkap      = isset($_POST['Nama_Lengkap'])?   $_POST['Nama_Lengkap'] :NULL;
            $Kata_Sandi      = isset($_POST['Kata_Sandi'])?   $_POST['Kata_Sandi'] :NULL;
			$Tanggal_Lahir       = $_POST['Tahun'].'-'.$_POST['Bulan'].'-'.$_POST['Tanggal'];
            $Jenis_Kelamin      = isset($_POST['Jenis_Kelamin'])?   $_POST['Jenis_Kelamin'] :NULL;
            $Alamat    = isset($_POST['Alamat'])? $_POST['Alamat']:NULL;
			$Divisi    = isset($_POST['Divisi'])? $_POST['Divisi']:NULL;
			$Posisi = isset($_POST['Posisi'])? $_POST['Posisi']:NULL;
           // $Id_Posisi    = isset($_POST['Id_Posisi'])? $_POST['Id_Posisi']:NULL;


            try {
                $this->payrollhrdmodel->insert($Nama_Pengguna, $Nama_Lengkap, $Kata_Sandi, $Tanggal_Lahir,$Jenis_Kelamin,$Alamat,$Divisi,$Posisi);
               $this->akhirisesi();
                return;
				
            } catch (\Exception $e) {

            }
        }

        include 'view/payrollhrd/add.php';
    }

    public function deleteGaji() {
		 $system= KomA::app();
        $id = isset($_GET['id'])?$_GET['id']:NULL;
        if ( !$id ) {
            throw new \Exception('Internal error.');
        }

        $this->payrollhrdmodel->delete($id);

        $this->akhirisesi();
    }

    public function showStaff() {
        $id = isset($_GET['id'])?$_GET['id']:NULL;
		$status = isset($_GET['status'])?$_GET['status']:NULL;
        if ( !$id ) {
            throw new \Exception('Internal error.');
        }
        $staff = $this->payrollhrdmodel->selectById($id,$status);

        include 'view/payrollhrd/profil_pegawai.php';
    }
	 public function showProfil() {

		$Nama_Pengguna = isset($_POST['Nama_Pengguna'])?$_POST['Nama_Pengguna']:NULL;
		$Kata_Sandi = isset($_POST['Kata_Sandi'])?$_POST['Kata_Sandi']:NULL;
		$report = isset($_GET['report'])?$_GET['report']:NULL;
		$id = isset($_GET['id'])?$_GET['id']:NULL;
		 session_start();
		if(isset($_SESSION['Nama_Pengguna']) && isset($_SESSION['Kata_Sandi'])){
			$Nama_Pengguna=$_SESSION['Nama_Pengguna'];
			$Kata_Sandi=$_SESSION['Kata_Sandi'];
		}

        if ( !$Nama_Pengguna || !$Kata_Sandi ) {
            throw new \Exception('Internal error.');
        }

        $staff = $this->payrollhrdmodel->selectForLogin($Nama_Pengguna,$Kata_Sandi,$report,$id);


		if($staff ===false){
			throw new \Exception('Nama_Pengguna and Kata_Sandi wrong');
		}
		else{
            if($staff->Akses_Level==0 || $staff->Akses_Level==2)
				include 'view/payrollhrd/data_pegawai.php';
			else
				$this->lists();
	}

    }

    public function showError($title, $message) {
        include 'view/error.php';
    }

    public function akhirisesi() {
		session_start();
        $system= KomA::app();
		$_SESSION['Nama_Pengguna'];
		$_SESSION['Kata_Sandi'];

		session_unset();

		session_destroy();
	    header('Location:'.$system->site_url('payrollhrd/index'));
	}
}

?>
