<?php

namespace Controller;

use Model\RecommendationModel;

class Recommendation
{
    public $model;

    public function defaultAction() { return 'index'; }

    public function __construct()
    {
        $this->model = new RecommendationModel();
    }

    public function index()
    {
        include 'view/maps/recommendation.php';
    }

    public function get_category() {
        echo json_encode($this->model->getCategoryList());
    }
    public function get_list() {
        if(empty($_GET['CatID']) || !isset($_GET['CatID']))
            $_GET['CatID'] = -1;
        echo json_encode($this->model->getByCategoryID($_GET['CatID']));
    }
}

?>
