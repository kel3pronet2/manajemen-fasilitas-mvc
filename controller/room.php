<?php

namespace Controller;

use Model\RoomModel;

class Room
{
    public $model;

    public function defaultAction() { return 'view'; }

    public function __construct()
    {
        $this->model = new RoomModel();
    }

    public function view($id = NULL)
    {
        if ($id == NULL) {
            $data = $this->model->getRoomTypeList();
            include 'view/room/room.php';
        } else {
            $data = $this->model->getRoomDetail($id);
            include 'view/room/roomdetail.php';
        }
    }

}

?>
