<?php

namespace Controller;

use Model\FacilityModel;
use System\KomA;

class Facility
{
    public $model;
	
    public function defaultAction() { return 'view'; }

    public function __construct()
    {
        $this->model = new FacilityModel();
    }

    public function view()
    {
        
            $data = $this->model->getFacility();
            include 'view/facility/facility.php';
       
    }
	
	public function viewReservasi()
    {
        
            $data = $this->model->getReservasi();
            include 'view/facility/listReservasi.php';
       
    }
	
	public function insertFacility()
	{
		$system= KomA::app();
		$nama_facility = '';
        $harga = '';
        $satuan = '';
        $deskripsi = '';
		$image = '';

        $errors = array();

        if ( isset($_POST['form-submited']) ) {
			
			
			
			$nama_facility = isset($_POST['nama_fasilitas']) ? $_POST['nama_fasilitas'] : null;
			$harga = isset($_POST['Harga']) ? $_POST['Harga'] : null;
			$satuan = isset($_POST['Satuan']) ? $_POST['Satuan'] : null;
			$deskripsi = isset($_POST['Deskripsi']) ? $_POST['Deskripsi'] : null;
			$lokasi_image = isset($_FILES['image']['tmp_name']) ? $_FILES['image']['tmp_name'] : null;
            $gambar = isset($_FILES['image']['name']) ? $_FILES['image']['name'] : null;
			$direktori = "../assets/images/facility/$gambar";
			
            try {
				$upload = move_uploaded_file($lokasi_image,$direktori);
                $this->model->inFacility($nama_facility, $harga, $satuan, $deskripsi,$gambar);
					
				header('Location:'.$system->site_url('facility/listFacility'));
				
            } catch (\Exception $e) {
					$errors = $e->getErrors();
					var_dump($errors);
            }
        }
		echo $direktori;
		include 'view/facility/insertfa.php';
	}
	
	public function updateFacility($id=NULL)
	{
		$system= KomA::app();
		$nama_facility = '';
        $harga = '';
        $satuan = '';
        $deskripsi = '';
		$image = '';

        $errors = array();

        if ( isset($_POST['form-submited']) ) {
			
			
			$nama_facility = isset($_POST['nama_fasilitas']) ? $_POST['nama_fasilitas'] : null;
			$harga = isset($_POST['Harga']) ? $_POST['Harga'] : null;
			$satuan = isset($_POST['Satuan']) ? $_POST['Satuan'] : null;
			$deskripsi = isset($_POST['Deskripsi']) ? $_POST['Deskripsi'] : null;
			$lokasi_image = isset($_FILES['image']['tmp_name']) ? $_FILES['image']['tmp_name'] : null;
            $gambar = isset($_FILES['image']['name']) ? $_FILES['image']['name'] : null;
			$direktori = "../assets/images/facility/$gambar";
			
            try {
				$upload = move_uploaded_file($lokasi_image,$direktori);
                $this->model->updateFacility($nama_facility, $harga, $satuan, $deskripsi,$gambar);
					
				header('Location:'.$system->site_url('facility/listFacility'));
				
            } catch (\Exception $e) {
					$errors = $e->getErrors();
					var_dump($errors);
            }
        }
		
		$data = $this->model->selectById1($id);
		include 'view/facility/updatefa.php';
	
	}
	
	public function listFacility()
	{
		
		$data = $this->model->getFacility();
		
		include 'view/facility/listfa.php';
	}
	
	public function deleteFacility($id)
	{
		$system= KomA::app();
		try{
			$this->model->deleteFacility($id);
			//header('Location:'.$system->site_url('facility/listFacility'));
		}catch (\Exception $e) {
					$errors = $e->getErrors();
					var_dump($errors);
            }
		
		
	}
}

?>
